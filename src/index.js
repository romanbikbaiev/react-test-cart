import * as React from "react";
import * as ReactDOM from "react-dom";
import {Route, Router, Switch} from 'react-router';
import {Provider} from 'react-redux'
import createBrowserHistory from 'history/createBrowserHistory'

import ProductList from "./containers/ProductList";
import Shipping from "./containers/Shipping";
import App from './components/App';
import store from "./store";

import './styles/index.scss';


// import * as serviceWorker from './serviceWorker';

const routing = (

    <Provider store={store}>
        <Router history={createBrowserHistory()}>
            <Switch>
                <Route exact path="/" component={App}/>
                <Route path="/cart" component={ProductList}/>
                <Route path="/shipping" component={Shipping}/>
                <Route component={App}/>
            </Switch>
        </Router>
    </Provider>

);

ReactDOM.render(routing, document.getElementById('root'));

// serviceWorker.unregister();
