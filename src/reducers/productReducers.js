import {
    DECREMENT_PRODUCT_COUNT, ERROR_IN_FETCH, INCREMENT_PRODUCT_COUNT, RECEIVE_PRODUCTS, REQUEST_PRODUCTS,
    TRASH_PRODUCT
} from "../constants/productActionTypes";

const initialState = {
    isError: false,
    isFetching: false,
    errorMessage: null,
    products: []
};

const productReducer = (state = initialState, action) => {
    switch (action.type) {
        case REQUEST_PRODUCTS:
            return {...state, isFetching: true};
        case RECEIVE_PRODUCTS:
            return {
                ...state,
                isFetching: false,
                isError: false,
                products: action.payload
            };
        case TRASH_PRODUCT:
        case DECREMENT_PRODUCT_COUNT:
        case INCREMENT_PRODUCT_COUNT:
            return {
                ...state,
                products: action.payload
            };
        case ERROR_IN_FETCH:
            return {
                ...state,
                isFetching: false,
                isError: true
            };
        default:
            return state;
    }
};

export default productReducer;