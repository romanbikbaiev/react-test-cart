import * as React from 'react';

export default class Price extends React.Component {

    render() {
        return (
            <div>
                {this.props.price} &euro;
            </div>
        )
    }
}