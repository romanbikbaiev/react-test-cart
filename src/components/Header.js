import * as React from 'react';

export default class Header extends React.Component {

    render() {
        return (
            <div className={'header'}>
                <div className={'topTitle'}>7ninjas</div>
                <div className={'subTitle'}>Front-End Developer</div>
            </div>
        )
    }

}