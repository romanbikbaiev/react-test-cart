import * as React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faTrash} from '@fortawesome/free-solid-svg-icons';
import Price from "./Price";

export default class Product extends React.Component {

    render() {
        const {id, title, subTitle, count, price} = this.props.product;
        const {
            incrementProductCount,
            decrementProductCount,
            trashProduct
        } = this.props;

        return (
            <div className={'product'}>
                <div className={'productImage'}>
                    <img src={'https://placekitten.com/100/100'} alt={title}/>
                </div>
                <div className={'productText'}>
                    <div className={'productTitle'}>{title}</div>
                    <div className={'productDescription'}>{subTitle}</div>
                </div>
                <div className={'productControls'}>
                    <div className={'decreaseProduct'} onClick={() => decrementProductCount(id)}>-</div>
                    <div className={'productCount'}>{count}</div>
                    <div className={'increaseProduct'} onClick={() => incrementProductCount(id)}>+</div>
                </div>
                <div className={'productActions'}>
                    <div className={'productPrice'}><Price price={count * price}/></div>
                    <div onClick={() => trashProduct(id)} className={'trashProduct'}><FontAwesomeIcon icon={faTrash}/>
                    </div>
                </div>
            </div>
        );
    }

}