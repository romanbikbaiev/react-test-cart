import {
    DECREMENT_PRODUCT_COUNT, ERROR_IN_FETCH, INCREMENT_PRODUCT_COUNT, RECEIVE_PRODUCTS, REQUEST_PRODUCTS,
    TRASH_PRODUCT
} from "../constants/productActionTypes";

export function fetchProducts() {
    return async (dispatch) => {
        dispatch({
            type: REQUEST_PRODUCTS
        });

        try {
            const response = await fetch('http://5c5d551bef282f0014c3d8f8.mockapi.io/api/v1/products');
            const json = await response.json();
            json.forEach(item => item.count = 1);

            dispatch({
                type: RECEIVE_PRODUCTS,
                payload: json
            });
        } catch(e) {
            dispatch({
                type: ERROR_IN_FETCH
            })
        }
    }
}

export function incrementProductCount(id) {

    return (dispatch, getState) => {
        const { products } = getState().products;
        const newProducts = [...products];
        newProducts.forEach((item) => item.count = item.id === id ? item.count + 1 : item.count)

        dispatch({
            type: INCREMENT_PRODUCT_COUNT,
            payload: newProducts
        });
    }
}

export function decrementProductCount(id) {
    return (dispatch, getState) => {
        const { products } = getState().products;
        const newProducts = [...products];
        newProducts.forEach((item) => item.count = item.id === id && item.count > 1 ? item.count - 1 : item.count);

        dispatch({
            type: DECREMENT_PRODUCT_COUNT,
            payload: newProducts
        });
    }
}

export function trashProduct(id) {
    return (dispatch, getState) => {
        const { products } = getState().products;
        const newProducts = products.filter((item) => item.id !== id)

        dispatch({
            type: TRASH_PRODUCT,
            payload: newProducts
        });
    }
}