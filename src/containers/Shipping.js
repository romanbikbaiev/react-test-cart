import * as React from 'react';
import {connect} from 'react-redux'
import Header from "../components/Header";

class Shipping extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            name: '',
            address: '',
            phone: '',
            email: '',
            shippingOption: '',
            isValid: false,
            errors: {
                name: true,
                address: true,
                email: true,
                phone: false
            }
        };

        // if (this.props.products.length === 0) {
        //     this.props.history.push('/cart');
        // }

        this.handleAddressChanged = this.handleAddressChanged.bind(this);
        this.handleNameChanged = this.handleNameChanged.bind(this);
        this.handlePhoneChanged = this.handlePhoneChanged.bind(this);
        this.handleEmailChanged = this.handleEmailChanged.bind(this);
        this.handleShippingOptionChanged = this.handleShippingOptionChanged.bind(this);
    }

    validateEmail(email) {
        // eslint-disable-next-line
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    validateForm() {
        const {name, address, phone, email, errors} = this.state;
        errors.name = name.length < 3;
        errors.address = address.length === 0;
        errors.phone = phone.length > 0 && phone.length !== 9;
        errors.email = !this.validateEmail(email);

        this.setState({
            errors,
            isValid: errors.name === false && errors.address === false && errors.phone === false && errors.email === false
        })
    }

    handleNameChanged(event) {
        this.setState({name: event.target.value}, () => this.validateForm());
    }

    handleAddressChanged(event) {
        this.setState({address: event.target.value}, () => this.validateForm());
    }

    handlePhoneChanged(event) {
        this.setState({phone: event.target.value}, () => this.validateForm());
    }

    handleEmailChanged(event) {
        this.setState({email: event.target.value}, () => this.validateForm());
    }

    handleShippingOptionChanged(event) {
        this.setState({shippingOption: event.target.value});
    }

    render() {
        const {products} = this.props;
        const {errors} = this.state;
        const totalSum = products.length > 0 ? products.reduce((result, item) => result + (item.count * item.price), 0) : 0;

        return (
            <div className={'container'}>
                <Header/>
                <div className={'body shippingCart'}>
                    <form>
                        <div className={'formRow'}>
                            <div className={'label'}>Name*</div>
                            <div>
                                <input type='text'
                                       className={errors.name ? 'invalid' : ''}
                                       placeholder={'Your name'}
                                       onChange={this.handleNameChanged}/>
                                {errors.name && (
                                    <span className={'errorText'}><br/>Invalid name</span>
                                )}
                            </div>
                        </div>

                        <div className={'formRow'}>
                            <div className={'label'}>Address*</div>
                            <div>
                                <input type='text'
                                       className={errors.address ? 'invalid' : ''}
                                       placeholder={'Your address'}
                                       onChange={this.handleAddressChanged}/>
                                {errors.address && (
                                    <span className={'errorText'}><br/>Invalid address</span>
                                )}
                            </div>
                        </div>

                        <div className={'formRow'}>
                            <div className={'label'}>Phone</div>
                            <div>
                                <input type='text'
                                       className={errors.phone ? 'invalid' : ''}
                                       placeholder={'123456789'}
                                       onChange={this.handlePhoneChanged}/>
                                {errors.phone && (
                                    <span className={'errorText'}><br/>Invalid phone</span>
                                )}
                            </div>
                        </div>

                        <div className={'formRow'}>
                            <div className={'label'}>E-mail*</div>
                            <div>
                                <input type='text'
                                       className={errors.email ? 'invalid' : ''}
                                       placeholder={'Your email'}
                                       onChange={this.handleEmailChanged}/>
                                {errors.email && (
                                    <span className={'errorText'}><br/>Invalid email</span>
                                )}
                            </div>
                        </div>

                        <div className={'formRow'}>
                            <div className={'label'}>Shipping options</div>
                            <div>
                                <select onChange={this.handleShippingOptionChanged}>
                                    <option value='ninjPost' disabled={products.length > 3}>ninjPost</option>
                                    <option value='D7L'>D7L</option>
                                    <option value='7post'>7post</option>
                                    {totalSum > 0 && (
                                        <option value='free'>Free shipping</option>
                                    )}
                                </select>
                            </div>
                        </div>

                        <div className={'formRow formShippingRow'}>
                            <div className={'shippingCartButton'}>
                                <button className={'button'}
                                        disabled={!this.state.isValid}
                                        onClick={(e) => e.preventDefault()}>Pay
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }

}


function mapStateToProps(state) {
    const {products} = state.products;

    return {
        products
    }
}

const mapDispatchToProps = dispatch => {
    //dispatch payment action
    return {}
};

export default connect(mapStateToProps, mapDispatchToProps)(Shipping);