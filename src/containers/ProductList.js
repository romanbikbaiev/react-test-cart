import * as React from 'react';
import {connect} from 'react-redux'
import {decrementProductCount, fetchProducts, incrementProductCount, trashProduct} from "../actions/productActions";
import Product from "../components/Product";
import Price from "../components/Price";
import Header from "../components/Header";

class ProductList extends React.Component {
    componentDidMount() {
        this.props.fetchProducts();
    }

    render() {
        const {products, isFetching, isError} = this.props;

        return (
            <div className={'container'}>
                <Header/>
                <div className={'body'}>
                    {!isFetching && products.length > 0 && (
                        <div>
                            {products.map((product) => (
                                <Product
                                    key={product.id}
                                    product={product}
                                    incrementProductCount={this.props.incrementProductCount}
                                    decrementProductCount={this.props.decrementProductCount}
                                    trashProduct={this.props.trashProduct}
                                />
                            ))}
                            <div className={'totalPriceRow'}>
                                <Price
                                    price={products.length > 0 && products.reduce((result, item) => result + (item.count * item.price), 0)}/>
                            </div>
                            <div className={'buyButtonRow'}>
                                <button className={'button'} onClick={() => this.props.history.push('/shipping')}>Buy
                                </button>
                            </div>
                        </div>
                    )}

                    {isFetching && (
                        <div className={'fetchingProducts'}>
                            Search for products in the card
                        </div>
                    )}

                    {isError && (
                        <div className={'errorInSearch'}>
                            Something went wrong! Try again
                        </div>
                    )}
                </div>
            </div>
        )
    }

}

function mapStateToProps(state) {
    const {products, isFetching, isError} = state.products;
    console.log(isFetching, isError, products);

    return {
        products,
        isFetching,
        isError
    }

}

const mapDispatchToProps = dispatch => {
    return {
        fetchProducts: () => {
            dispatch(fetchProducts());
        },
        incrementProductCount: (id) => {
            dispatch(incrementProductCount(id));
        },
        decrementProductCount: (id) => {
            dispatch(decrementProductCount(id));
        },
        trashProduct: (id) => {
            dispatch(trashProduct(id));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductList);